import { Component, OnInit, style, state, trigger, transition, animate  } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../services/data/data.service';
import { SortListService } from '../services/sort-list/sort-list.service';
import {PageScrollConfig} from 'ng2-page-scroll';
import { AppComponent } from '../app.component';
import { List } from '../models/list';
import { Invitee } from '../models/invitee';

import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.component.html',
  styleUrls: ['./check-in.component.scss'],
  animations: [
    trigger('shrink', [
      transition('* => void', [
        style({height: '*'}),
        animate(250, style({height: 0, opacity: 0}))
      ])
    ])
  ]
})
export class CheckInComponent implements OnInit {

  selectedEvent: any;
  alphabet: Set<string>;
  needCheckedIn: List = {list: new Array(), alphabet: new Set(), flip: new Array()};
  waitingOnWaiver: List = {list: new Array(), alphabet: new Set(), flip: new Array()};;
  completed: List = {list: new Array(), alphabet: new Set(), flip: new Array()};;
  waiverComplete: boolean = false;
  waiverUnsigned: boolean = false;
  waiverBoth: boolean = true;
  tabs = {t1: true, t2: false, t3: false};
  inviteeNumber: number;
  showTempPanel: boolean = false;

  constructor(private router:Router, private data:DataService, private route: ActivatedRoute, private parent: AppComponent, private list: SortListService) { 
    PageScrollConfig.defaultScrollOffset = 10;
    PageScrollConfig.defaultEasingLogic = {  
      ease: (t: number, b: number, c: number, d: number): number => {
        // easeInOutExpo easing 
        if (t === 0) return b; if (t === d) return b + c; if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
      } 
    }
  }

  ngOnInit() {
    this.buildFirstList();
    this.changeTitle();
  }

  changeTitle():void {
    this.parent.newTitle(': Check These Fools In', true);
  }
  
  buildFirstList():void {
    let id = this.route.snapshot.params['id'];
    console.log(id);
    this.selectedEvent = this.data.getSingleEvent(id); //These names must come in sorted alphabetically, starting with 'A'
    this.needCheckedIn.list = this.selectedEvent.invitees;
    this.needCheckedIn = this.addHeadersToList(this.needCheckedIn);
    this.alphabet = this.needCheckedIn.alphabet;
    this.inviteeNumber = this.guestCount(this.needCheckedIn.list);
    };

  addHeadersToList(input): List {
    let temp = [];
    for(let indv of input.list){
      if(indv.name){
        let letter = indv.name.substr(0, 1);
        if (!input.alphabet.has(letter)){
          temp.push({'header':letter});
          input.flip.push(false);
        }
        input.alphabet.add(letter);
        temp.push(indv);
        input.flip.push(false);
      } else {
        temp.push(indv);
      }
    }
    input.list = temp;
    return input;
  }

  removeHeadersFromList(input): List {
    let value1 = false;
    let value2 = false;
    let prev: number;
    for(let i=0; i < input.list.length; i++){
      (i-1 < 0) ? prev = 0 : prev = i-1;
      if(i%2 == 0){
        (input.list[i].header) ? value1 = true : value1 = false;
      } else {
        (input.list[i].header) ? value2 = true : value2 = false;
      }
      if(value1 && value2){
        input.alphabet.delete(input.list[prev].header);
        input.flip.splice(prev, 1);
        input.list.splice(prev, 1);
        i = i-1;
      }
    }
    return input;
  }

  flipMe(index): void{
    this.needCheckedIn.flip.fill(false);
    this.needCheckedIn.flip[index] = !this.needCheckedIn.flip[index];
  }

  changeView(el):void {
    //this can be done in a better way. Maybe on the html completely
    let id = el.path[0].id;
    if(id == 't1') {
      this.tabs.t1 = true;
      this.tabs.t2 = false;
      this.tabs.t3 = false;
      this.alphabet = this.needCheckedIn.alphabet;
      this.inviteeNumber = this.guestCount(this.needCheckedIn.list);
    } else if(id == 't2') {
     this.tabs.t1 = false;
      this.tabs.t2 = true;
      this.tabs.t3 = false;
      this.alphabet = this.waitingOnWaiver.alphabet;
      this.inviteeNumber = this.guestCount(this.waitingOnWaiver.list);
    } else if(id == 't3') {
      this.tabs.t1 = false;
      this.tabs.t2 = false;
      this.tabs.t3 = true;
      this.alphabet = this.completed.alphabet;
      this.inviteeNumber = this.guestCount(this.completed.list);
    } else {
      this.tabs.t1 = true;
      this.tabs.t2 = false;
      this.tabs.t3 = false;
      this.alphabet = this.needCheckedIn.alphabet;
      this.inviteeNumber = this.guestCount(this.needCheckedIn.list);
    }
    console.log(this.alphabet);
  }

  sendWaiver(invId):void {
    let waiverUrl = `waiver.campeagle.camp/event/${this.selectedEvent.id}`;
    let index = this.needCheckedIn.list.findIndex( (list) => list.id == invId);
    let inv = this.needCheckedIn.list[index];
    if(inv.type == 'adult'){
      waiverUrl = waiverUrl + `?type=adult&email=${inv.email}`;
    } else {
      // spaces in the name are fine in this case
      waiverUrl = waiverUrl + `?type=child&name=${inv.name}&email=${inv.email}`
    }
    //send email with waiver url 
    console.log(waiverUrl);
    if(index != -1){
      this.waitingOnWaiver.list.push(this.needCheckedIn.list[index]);
      this.needCheckedIn.list[index].id = '-1';
      setTimeout( () => {
        this.needCheckedIn.list.splice(index, 1);
        this.needCheckedIn.flip.splice(index, 1);
        this.waitingOnWaiver = this.addHeadersToList(this.waitingOnWaiver);
        this.needCheckedIn = this.removeHeadersFromList(this.needCheckedIn);
        this.needCheckedIn.flip.fill(false);
        this.inviteeNumber = this.guestCount(this.needCheckedIn.list);
      }, 1000);
    }
  }

  checkIn(invId):void {
    let index = this.needCheckedIn.list.findIndex( (list) => list.id == invId);
    if(index != -1){
      this.completed.list.push(this.needCheckedIn.list[index]);
      this.needCheckedIn.list[index].id = '-1';
      setTimeout( () => {
        this.needCheckedIn.list.splice(index, 1);
        this.needCheckedIn.flip.splice(index, 1);
        this.completed = this.addHeadersToList(this.completed);
        this.needCheckedIn = this.removeHeadersFromList(this.needCheckedIn);
        this.needCheckedIn.flip.fill(false);
        this.inviteeNumber = this.guestCount(this.needCheckedIn.list);
      }, 1000);
    }
  }
  
  guestCount(list): number{
    let listlength = 0;
    for(let indv of list){
      if(!indv.header){
        listlength++;
      }
    }
    return listlength;
  }

}
