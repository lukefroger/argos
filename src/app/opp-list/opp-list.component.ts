import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data/data.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-opp-list',
  templateUrl: './opp-list.component.html',
  styleUrls: ['./opp-list.component.scss'],
})
export class OppListComponent implements OnInit {

  dateForToday = new Date().toDateString();
  programs:Set<any> = new Set();
  events: Array<any> = [];
  otherEvents: Array<any> = [];
  program: string = "none";
  start: string;

  user = {name:'luke frog', program: 'retreats'}; 

  constructor(private router:Router, private data:DataService, private parent: AppComponent) {
   }

  ngOnInit() {
    this.getEvents();
    this.setDefaults();
    this.changeTitle();
  }

  changeTitle(): void{
    this.parent.newTitle(': Event List', false);
  }

  getEvents(): void {
   let _allEvents = this.data.getEvents();
    for(let event of _allEvents){
      this.programs.add(event.program);
      if(event.program == this.user.program){
        this.events.push(event);
      } else {
        this.otherEvents.push(event);
      }
    }
  }
  
  setDefaults(): void {
    let _temp = new Date().toDateString().split(' ');
    let _month = new Date().getMonth() + 1;
    this.start = `${_temp[3]}-${_month}-${_temp[2]}`;
  }

  updateList(): void {
    // make a pipe and re-code
    let _updatedList = [];
    console.log(this.start);
    for(let event of this.otherEvents){
      if(event.program == this.program && event.start == this.start){
        _updatedList.push(event);
      }
    }
    _updatedList = this.otherEvents;
  }

  checkInThisEvent(id: string): void {
    this.router.navigate(['/check-in', id]);
  }


}
