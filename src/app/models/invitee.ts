export class Invitee{
    id: string;
    name: string;
    type: string;
    completed_requirements: string;
    email: string;
}