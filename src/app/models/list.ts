import { Invitee } from './invitee';

export class List{
    list: Array<Invitee>;
    alphabet: Set<string>;
    flip: Array<boolean>;
}