import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  dateForToday = new Date().toDateString();
  constructor() { }

  getEvents(): Array<any>{
    let opps = 
    [
      {
        id: 1,
        name: 'G2 - Retreat - Territory Cowboy Church Ski Retreat - 3/2017',
        date: `${this.dateForToday}`,
        length: '6',
        program: 'retreats'
      },
      {
        id: 2,
        name: 'Glorieta Group Camp Session 2 - Christ Community Church Tucson',
        date: `${this.dateForToday}`,
        length: '4',
        program: 'group camp'
      },
      {
        id: 3,
        name: 'Stonegate Marriage Retreat - Joe Russo - April 2017',
        date: `${this.dateForToday}`,
        length: '2',
        program: 'retreats'
      },
      {
        id: 4,
        name: 'Collegiate Week 2016 Aug 7th thru 12th',
        date: `${this.dateForToday}`,
        length: '6',
        program: 'group camp'
      },
      {
        id: 5,
        name: 'Teen Twice the Adventure #2 2016 - Waugh Household',
        date: `${this.dateForToday}`,
        length: '10',
        program: 'Individual Camp'
      },
      {
        id: 6,
        name: 'West Texas A&M Ski Trip - Bill Banks - February 2017',
        date: `${this.dateForToday}`,
        length: '3',
        program: 'retreats'
      },
      {
        id: 7,
        name: 'Family Camp Session 5, 2016 - Wheeler Household',
        date: `${this.dateForToday}`,
        length: '7',
        program: 'family camp'
      },
      {
        id: 8,
        name: 'Passport: Glorieta Student Conference 2017 for Groups',
        date: `${this.dateForToday}`,
        length: '6',
        program: 'conferences'
      },
      {
        id: 9,
        name: 'Teen Twice the Adventure #2 2016 - Waugh Household',
        date: `${this.dateForToday}`,
        length: '10',
        program: 'Individual Camp'
      },
      {
        id: 10,
        name: 'West Texas A&M Ski Trip - Bill Banks - February 2017',
        date: `${this.dateForToday}`,
        length: '3',
        program: 'retreats'
      },
      {
        id: 11,
        name: 'Family Camp Session 5, 2016 - Wheeler Household',
        date: `${this.dateForToday}`,
        length: '7',
        program: 'family camp'
      },
      {
        id: 12,
        name: 'Passport: Glorieta Student Conference 2017 for Groups',
        date: `${this.dateForToday}`,
        length: '6',
        program: 'conferences'
      }
    ]
    return opps;
  }

  getSingleEvent(oppId): Object{
    console.log('event here');
    let query = {
      public_name: "West Texas A&M Ski Trip - Bill Banks - February 2017",
      registered_guests: 21,
      id: oppId,
      invitees: [
        {id: "1a", name: 'Ashely Gray', type: 'child', completed_requirements: '', email: 'luke@campeagle.org'},
        {id: "2a", name: 'Alex Martiniz', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "3a", name: 'Beth Minsterson', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "4a", name: 'Brian Odriscol', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "5a", name: 'Ben Whyet', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "6a", name: 'Chris Hemsworth', type: 'adult', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "7a", name: 'Drew Beberstien', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "8a", name: 'Frankie Delarose', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "9a", name: 'Georgia d Dog', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "1a0", name: 'Hermironie Granger', type: 'adult', completed_requirements: '', email: 'luke@campeagle.org'},
        {id: "1a1", name: 'Kelly Gurken', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "1a2", name: 'Logan Wolverine', type: 'adult', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "1a3", name: 'Luke Frauhiger', type: 'adult', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "1a4", name: 'Numan Portage', type: 'child', completed_requirements: '', email: 'luke@campeagle.org'},
        {id: "1a5", name: 'Quinn Funk', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "1a6", name: 'Ronald Minsterson', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "1a7", name: 'Sarah Minsterson', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "1a8", name: 'Sheqila Nuo', type: 'child', completed_requirements: '', email: 'luke@campeagle.org'},
        {id: "1a9", name: 'SunnyBill Willions', type: 'child', completed_requirements: '', email: 'luke@campeagle.org'},
        {id: "2a0", name: 'Umbridge Pufl', type: 'adult', completed_requirements: 'waiver', email: 'luke@campeagle.org'},
        {id: "2a1", name: 'Yeven McFurington', type: 'child', completed_requirements: 'waiver', email: 'luke@campeagle.org'}
      ]
    }
    return query;
  }
  
}
