import { Injectable } from '@angular/core';

@Injectable()
export class SortListService {

  constructor() { }

   addHeadersToList(newList: Array<any>, masterList: Array<any>) {
    let alphabet = new Set();
    for(let indv of masterList){
      let letter = indv.name.substr(0, 1);
      if (!alphabet.has(letter)){
        newList.push({'header':letter});
      }
      alphabet.add(letter);
      newList.push(indv);
    }
    let lists = {newList, masterList};
    return lists;
  }

}
