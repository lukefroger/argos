/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SortListService } from './sort-list.service';

describe('Service: SortList', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SortListService]
    });
  });

  it('should ...', inject([SortListService], (service: SortListService) => {
    expect(service).toBeTruthy();
  }));
});
