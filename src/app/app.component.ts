import { Component, Input } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  
})
export class AppComponent {
  title: string = '';
  goBack: Boolean = false;

  constructor(private location: Location){
    console.log('title');
  }

  newTitle(title: string, back: Boolean){
    this.title = title;
    this.goBack = back;
  }

  back():void{
    this.location.back();
  }
  
}
