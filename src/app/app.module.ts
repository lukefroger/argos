import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { Ng2PageScrollModule } from 'ng2-page-scroll';

/* navigation */
import { AppRoutingModule } from './app-routing.module';

/*pages*/
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { OppListComponent } from './opp-list/opp-list.component';
import { CheckInComponent } from './check-in/check-in.component';

/*services*/
import { DataService } from './services/data/data.service';
import { SortListService } from './services/sort-list/sort-list.service';


@NgModule({
  declarations: [
    AppComponent,
    OppListComponent,
    LoginComponent,
    CheckInComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    Ng2PageScrollModule.forRoot()
  ],
  providers: [DataService, SortListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
