import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Pages */
import { OppListComponent } from './opp-list/opp-list.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { CheckInComponent } from './check-in/check-in.component';


const routes:Routes = [
    {
        path: 'event-list',
        component: OppListComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'check-in/:id',
        component: CheckInComponent
    },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

