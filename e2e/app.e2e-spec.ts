import { CheckinPage } from './app.po';

describe('checkin App', function() {
  let page: CheckinPage;

  beforeEach(() => {
    page = new CheckinPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
